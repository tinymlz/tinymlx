# TinyMLz on TinyMLx

_A hopelessly biased and brutally honest review of [Harvard's TinyML course on edX](https://www.edx.org/professional-certificate/harvardx-tiny-machine-learning)_

**TL;DR good intro-level course, advanced learners need not apply**

|**Prediction**|
|---|
| _Just like mobile phones of yore, TinyML will start to suck less when the devices become sufficiently capable to actually fulfill all the promises being made. For some use cases, that may be today or tomorrow; for others, nobody knows..._ |

## Biases
* I got my PhD a _loooong_ time ago, and have been a life-long learner ever since
  - most MOOC:s strike me as too low quality and too high-level to be truly useful
* I do AI/ML for a living
  - or, I did, until I got disillusioned and moved on
  - but, I might return one day as the general area still fascinates me
* I'm _quite_ familiar with embedded Linux, hacking Android phones and tablets
  - no prior experience with microcontrollers, though
* I **audited** all three courses _on my own time_ **for free**
  - no stakes, other than my passtime invested


## The Good

Vijay is _excellent_!  He's probably the sole reason, beyond my own curiosity, that I did actually complete all three courses, despite early signs that I wasn't going to get much out of them.  Actually, he managed to keep me excited about learning _something_ cool in the last course almost until the very end, when it became painfully obvious that that wasn't going to happen.

All three courses are well-structured, thoughtfully put together, *and* truly open as evidenced by the wealth of publicly available [resources](#Resources).

The [course kit](https://store.arduino.cc/tiny-machine-learning-kit) has clear potential.


## The Bad

The pace is fairly slow.  I watched most of the videos at 1.5x, and that's because that was the highest speed available when I took the first two courses.  On the third course, 2x was added. This turned out to be perfect for Brian's boring labs, and would've been a godsend for Moroney's  "high-school STEM" lectures, but ended up being a tad too fast for Vijay.  He remained pegged at 1.5x, although I suspect that 1.75x would've been better, had it been available.

Some of the so-called "lecturers" _read_ straight from a script.  The content is OK, but the delivery leaves a lot to be desired.  Audience engagement tends to zero.  Boredom ensues.  At least, the speed is somewhat higher...  Susanne does this quite blatantly; Pete in a slightly more subtle manner. While they are both guilty, the former performer's efforts are nuanced by a sophisticated eye-candy effect. The same cannot be said about our old hippie friend. 

Everything is spoon-fed.  The most complicated exercise requires copy&paste skills.  Thinking or problem-solving is not required.

The [course kit](https://store.arduino.cc/tiny-machine-learning-kit)'s potential remains largely unexploited during the course.  Vijay does encourage students to create their own projects to get some truly hands-on experience.  They even have a [competition](https://discuss.tinymlx.org/t/tinymlx-course-3-post-1-call-for-tinyml-edx-projects-guidelines/410)!


## The Ugly

This is _primarily_ a course on Tensorflow Lite Micro and **not** on TinyML in general, although arguably the relatively self-evident "principles" should be widely applicable.

Shameless plugs for Google's tooling are all over the place.  There is [**one** slide (#13/19)](https://github.com/tinyMLx/courseware/blob/master/edX/slides/4-3-10.pdf) during the entire programme briefly mentioning alternative tools and frameworks.  Perhaps, it's worth listing those here as well.

* [microTVM](https://tvm.apache.org/docs/microtvm/index.html) of [TVM fame](https://github.com/apache/tvm/)
* [uTensor](https://github.com/uTensor/uTensor)
* [Glow](https://github.com/pytorch/glow) of [PyTorch fame](https://github.com/pytorch)
* [STM32Cube.AI](https://www.st.com/content/st_com/en/ecosystems/stm32-ann.html)
  - this gets 3 more slides including a comparison with TFLite (**not** Micro)

## Resources

* [TinyML Programme](https://www.edx.org/professional-certificate/harvardx-tiny-machine-learning)
  - [Fundamentals of TinyML](https://www.edx.org/course/fundamentals-of-tinyml)
  - [Applications of TinyML](https://www.edx.org/course/applications-of-tinyml)
  - [Deploying TinyML](https://www.edx.org/course/deploying-tinyml)
* [Course kit](https://store.arduino.cc/tiny-machine-learning-kit)
* [GitHub group](https://github.com/TinyMLx) including
  - all [courseware](https://github.com/tinyMLx/courseware)
  - all [colabs](https://github.com/tinyMLx/colabs)
    * these are also hosted on [Colab](https://colab.research.google.com/github/tinyMLx)
  - [Arduino library](https://github.com/tinyMLx/arduino-library)
  - [community website](https://github.com/tinyMLx/tinyMLx.github.io) (see below)
  - and more...
* [TinyMLx Community](https://tinymlx.org/) with its own [Discourse](https://discuss.tinymlx.org/)
* [TinyMLPerf benchmarks](https://github.com/mlcommons/tiny)

### Media
* [InformationWeek Commentary](https://www.informationweek.com/big-data/ai-machine-learning/google-harvard-and-edx-team-up-to-offer-tinyml-training/a/d-id/1338648?)
* [GoogleNews Post](https://www.googlenewsapp.com/the-future-of-machine-learning-is-tiny-and-bright/)
* [reddit attempt](https://www.reddit.com/r/learnmachinelearning/comments/jyfodb/course_on_tinyml/)
